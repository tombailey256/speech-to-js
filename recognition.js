const context = {
  lineNumber: 1,
};
const regexToTranspiler = {
  "define (an )?array (called )?(?<arrayName>\\w+)": match => `const ${match.groups.arrayName} = [];`,
  "add (?<item>\\w+) to (?<arrayName>\\w+)": match => `${match.groups.arrayName}.push("${match.groups.item}");`,
  "for each (?<itemName>\\w+) in (?<arrayName>\\w+)": match => ({
    code: `${match.groups.arrayName}.forEach((${match.groups.itemName}) => {\n});`,
    isBlock: true
  }),
  "show (?<variable>\\w+)": match => `alert(${match.groups.variable});`,
};

function init() {
  document.getElementById("run").addEventListener("click", runCode);
  startSpeechRecognition();
}

function runCode() {
  log("Running code");
  eval(document.getElementById("code").innerText);
}

function startSpeechRecognition() {
  const recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.lang = "en";
  recognition.onstart = started;
  recognition.onresult = processSpeech;
  recognition.onerror = processError;
  recognition.onend = ended;
  recognition.start();
}

function started() {
  log("Started listening");
}

function processSpeech(event) {
  const speechResult = event.results[event.resultIndex];
  if (speechResult.isFinal) {
    const text = speechResult[0].transcript;
    log(`You said '${text}'`);
    processInput(text);
  }
}

function processInput(input) {
  const normalisedInput = input.trim();
  const regex = Object.keys(regexToTranspiler).find((regex) => {
    return normalisedInput.match(new RegExp(regex, "i"));
  });
  if (regex) {
    showInput(input);

    const output = regexToTranspiler[regex](normalisedInput.match(new RegExp(regex, "i")));
    if (typeof output == 'string') {
      addCode(output);
    } else {
      addCode(output.code, output.isBlock);
    }
  } else {
    log(`Syntax error, '${normalisedInput}' is not recognised.`);
  }
}

function showInput(input) {
  document.getElementById("input").innerText += input + "\n";
}

function addCode(newCode) {
  const currentCode = document.getElementById("code").innerText;
  const currentLines = currentCode.split("\n");
  const newLines = currentLines.slice(0, context.lineNumber - 1)
    .concat([newCode])
    .concat(currentLines.slice(context.lineNumber - 1, currentLines.length));
  document.getElementById("code").innerText = newLines.join("\n");
  context.lineNumber++;
}

function processError(event) {
  log(`Speech recognition error. error: '${event.error}', message: '${event.message}'`);
}

function ended() {
  log("Stopped listening");
  startSpeechRecognition();
}

function log(message) {
  console.log(message);
  document.getElementById("debug").innerText += message + "\n";
}

init();
