# Speech To JS

## What is it?

It is a web-based development environment where you talk to generate JavaScript (JS) code. It is a very naive transpiler from spoken English to JS. At the moment, the project is only a prototype so it has very limited functionality. You can:

 * define arrays -> say `define array {name}`
 * add items to arrays -> say `add {item} to {array_name}`
 * loop through arrays -> say `for each {item_name} in {array_name}`
 * display those items -> say `show {item_name}`

A live demonstration is available [here](http://tombaileys.life/talkingcode).

A video demonstration is available on [YouTube](https://youtu.be/huo4Bi-gSXo).

## Why?

This project is mainly a thought experiment turned into a Proof of Concept (PoC) but a future version could be helpful for:

* mobile users that find it difficult or slow to type JS code
* people that suffer from Repetitive Strain Injury (RSI) or other medical issues that make typing difficult/impossible
* people that have little or no previous experience programming

## How does it work?

The [Web Speech API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API) is used to convert speech to text. The text is then run through a series of regular expressions to understand what code should be produced. For example, the regular expression `add (?<item>\\w+) to (?<arrayName>\\w+)` matches the phrase `add milk to shopping` to generate the code `shopping.push("milk");`.

## Limitations

The Web Speech API isn't available in all browsers. Even if it was, most implementations aren't tuned for programming vocabulary which leads to mis-recognition of phrases. In the future, it would be better to use a custom text to speech model to understand the phrases spoken as it could be tuned for the programming vocabulary. It would be ideal to run such a model locally, within the browser, to avoid the latency and privacy issues that arise from sending speech to a backend server.

Additionally, regular expressions are used to convert spoken phrases into code. This implementation is simplistic but brittle as it expects very specific phrases. A better approach would be to train a Natural Language Processing (NLP) model on a varied set of phrases. Perhaps unrecognised phrases could be sent to a backend server for continuous tweaking of the model if user consent was obtained.

## Can I use/extend this?

Yes, please do, as long as you follow the license terms (MIT).
